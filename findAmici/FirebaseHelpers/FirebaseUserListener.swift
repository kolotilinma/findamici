//
//  FirebaseUserListener.swift
//  findAmici
//
//  Created by Михаил on 11.11.2020.
//

import Foundation
import Firebase

class FirebaseUserListener {
    
    static let shared = FirebaseUserListener()
    
    private init() { }
 
    //MARK: - Resent link methods
    func resendVerificationEmail(email: String, completion: @escaping (_ error: Error?) -> Void) {
        Auth.auth().currentUser?.reload(completion: { (error) in
            Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                completion(error)
            })
        })
    }
    
    func resetPasswordFor(email: String, completion: @escaping (_ error: Error?) -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            completion(error)
        }
    }
    
    //MARK: - Login
    func loginUserWith(email: String, password: String, completion: @escaping (_ error: Error?, _ isEmailVerified: Bool) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if error == nil && authResult!.user.isEmailVerified {
                FirebaseUserListener.shared.dowloadUserFromFirebase(userId: authResult!.user.uid, email: email)
                
                completion(error, true)
            } else {
                print("Email is not verified")
                completion(error, false)
            }
        }
    }
    
    //MARK: - Register
    func registerUserWith(email: String, password: String, completion: @escaping (_ error: Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            if let error = error {
                completion(error)
            } else {
                guard let firebaseUser = authResult?.user else { return }
                firebaseUser.sendEmailVerification(completion: { (error) in
                    if error != nil {
                        completion(error)
                        print("Auth email sent with error: \(error!.localizedDescription)")
                    }
                })
                let user = User(id: authResult!.user.uid, username: email, email: email, pushId: "", avatarLink: "", status: "Hey there I'm using findAmici!")
                saveUserLocally(user)
                self.saveUserToFirestore(user)
                
            }
        }
    }
    
    //MARK: - Save Users
    func saveUserToFirestore(_ user: User) {
        do {
            try FirebaseReferance(.User).document(user.id).setData(from: user)
        } catch let err {
            print(err.localizedDescription)
        }
    }
    
    //MARK: - DowloadUserFromFirebase
    func dowloadUserFromFirebase(userId: String, email: String? = nil) {
        FirebaseReferance(.User).document(userId).getDocument { (querySnapshot, error) in
            guard let document = querySnapshot else {
                print("no document for user")
                return
            }
            let result = Result {
                try? document.data(as: User.self)
            }
            
            switch result {
            case .success(let userObject):
                if let user = userObject {
                    saveUserLocally(user)
                } else {
                    print("Document does not exist")
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}
